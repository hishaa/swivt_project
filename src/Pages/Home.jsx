import React from "react";
import { Banner } from "../Components/Banner/Banner";
import Catalog from "../Components/Catalog/Catalog";
import Coupen from "../Components/Coupen/Coupen";
import Cataloglist from "../Components/Catalog/Cataloglist";
import Carousel from "../Components/Carousel/Carousel";
import Deals from "../Components/Deals/Deals";
import Deallist from "../Components/Deals/Deallist";
import { Link } from "react-router-dom";
import Image from "../Components/Assets/Images/9.png";
import Image1 from "../Components/Assets/Images/image.png";
import Image3 from "../Components/Assets/Images/delivery2.png";
import Image4 from "../Components/Assets/Images/qr.png";
import Popularcarousel from "../Components/PopularFood/Popularcarousel";
import Promotion from "../Components/Promotion/Promotion";
import Offers from "../Components/Offers/Offers";
import Breakfastitem from "../Components/Breakfastitem/Breakfastitem";
import Offerlist from "../Components/Offers/Offerlist";
import Image5 from "../Components/Assets/Images/g.png";
import Image6 from "../Components/Assets/Images/a.png";
import Image7 from "../Components/Assets/Images/Rectangle.png";

const Home = () => {
  return (
    <>
      <div className="home">
        <Banner />
        <div className="coupon_contents container mt-4">
          <div className="headings">
            <h2>Coupon code</h2>
          </div>
          <div className="row">
            <div className="col-lg-3 col-md-3  ">
              <Coupen
                offpercent="30% off"
                code="MONY20"
                backgroundcolor="#FCBC08"
              />
            </div>
            <div className="col-lg-3 col-md-3">
              <Coupen
                offpercent="35% off"
                code="MONY20"
                backgroundcolor="#EFB55D"
              />
            </div>
            <div className="col-lg-3 col-md-3 ">
              <Coupen
                offpercent="10% off"
                code="MONY20"
                backgroundcolor="#926228"
              />
            </div>
            <div className="col-lg-3 col-md-3">
              <Coupen
                offpercent="5% off"
                code="MONY20"
                backgroundcolor="#EDA3BF"
              />
            </div>
          </div>
        </div>
        <div className="catalog_contents container mt-4">
          <div className="headings">
            <h2>
              Catalog section <br />
              delicacies
            </h2>
          </div>
          <div className="row">
            {Cataloglist.map((list) => {
              return (
                <div className="col">
                  <Catalog image={list.image} text={list.name} />
                </div>
              );
            })}
          </div>
        </div>
        <div className="carousel-contents container mt-4">
          <Carousel />
        </div>
        <div className="deals_content container mt-4">
          <div className="headings">
            <h2>best deals</h2>
          </div>
          <div className="row">
            {Deallist.map((item) => {
              return (
                <div className="col-lg-4 col-md-4">
                  <Deals image={item.image} />
                </div>
              );
            })}
          </div>
        </div>
        <div className="popular_food container mt-4">
          <div className="top d-flex">
            <div className="headings">
              <h2>Popular foods</h2>
            </div>
            <div className="headings">
              <a href="#">
                <button className="btn">See All</button>
              </a>
            </div>
          </div>
          <Popularcarousel />
        </div>
        <div className="popular_food container mt-4">
          <div className="top d-flex">
            <div className="headings">
              <h2>Breakfast items</h2>
            </div>
            <div className="headings">
              <a href="#">
                <button className="btn">See All</button>
              </a>
            </div>
          </div>
          <Breakfastitem />
        </div>
        <div className="popular_food container mt-4">
          <div className="top d-flex">
            <div className="headings">
              <h2>Drinks items</h2>
            </div>
            <div className="headings">
              <a href="#">
                <button className="btn">See All</button>
              </a>
            </div>
          </div>
          <Breakfastitem />
        </div>
        <div className="popular_delicacies container mt-4">
          <div className="headings">
            <h2>Popular delicacies</h2>
          </div>
          <div className="row">
            <div className="col-lg-6">
              <div className="deilcacies_image">
                <img src={Image} alt="delicacies" />
              </div>
            </div>
            <div className="col-lg-6 delicacy">
              <div className="delicacy_desc">
                <h3>Simple, tasty and very healthy vegetable and egg salad</h3>
                <h6>Recipe:</h6>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam
                  suscipit asperiores tenetur dolorem cumque minima iusto
                  voluptate dolor, in, laudantium eius autem numquam id amet
                  nemo quaerat! Ratione, ut perferendis.Lorem ipsum dolor sit
                  amet consectetur adipisicing elit. Ullam suscipit asperiores
                  tenetur dolorem cumque minima iusto voluptate dolor, in,
                  laudantium eius autem numquam id amet nemo quaerat! Ratione,
                  ut perferendis.
                </p>
              </div>
              <div className="delicacy_add row">
                <div className="col-md-6">
                  <button type="button" class="btn btn-warning">
                    Read Completely
                  </button>
                </div>

                <div class="qty col-md-6 d-flex">
                  <h6>quantity</h6>
                  <span className="minus">-</span>
                  <input type="number" className="count" name="qty" value="1" />
                  <span className="plus">+</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="company container mt-4">
          <div className="row">
            <div className="col-md-6">
              <div className="headings">
                <h2>
                  <span>about</span> company
                </h2>
              </div>
              <div className="company_desc">
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem
                  sapiente amet consequatur tempore modi ducimus earum, labore,
                  pariatur, atque voluptate ratione iure voluptatum similique
                  molestiae. In possimus quisquam ad inventore!Lorem ipsum dolor
                  sit amet consectetur adipisicing elit. Autem sapiente amet
                  consequatur tempore modi ducimus earum, labore, pariatur,
                  atque voluptate ratione iure voluptatum similique molestiae.
                  In possimus quisquam ad inventore!
                </p>
              </div>
            </div>
            <div className="col-md-6">
              <div className="company_video ps-4">
                <iframe
                  className="video"
                  src="https://www.youtube.com/embed/HxdYQrWbWDg"
                  title="YouTube video player"
                  frameborder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                  allowfullscreen
                ></iframe>
              </div>
            </div>
          </div>
        </div>
        <div className="h_banner">
          <img src={Image1} alt="banner image" />
        </div>
        <div className="promotion container mt-4">
          <div className="row">
            <div className="col-md-6 p_class">
              <Promotion />
            </div>
            <div className="col-md-6 p_class">
              <Promotion />
            </div>
          </div>
        </div>
        <div className="offers container mt-4">
          <div className="row">
            {Offerlist.map((item) => {
              return (
                <div className="col-md-3">
                  <Offers
                    image={item.image}
                    title={item.title}
                    description={item.description}
                  />
                </div>
              );
            })}
          </div>
        </div>
        <div className="program">
          <div className="row">
            <div className="col-md-5">
              <div className="program_image">
                <img src={Image3} alt="delivery rider" />
              </div>
            </div>
            <div className="col-md-5">
              <div className="program_detail pt-4">
                <h4>Join our</h4>
                <h1>Rider program</h1>
                <p>
                  Citi977’s rider crew is the backbone of Citi977 vision to
                  change how people get their Grocery & Foods
                </p>
                <p>Be the Citi977 Delivery Hero and earn more!</p>
                <button className="btn btn-sm"> Apply</button>
              </div>
            </div>
          </div>
        </div>
        <div className="payment container">
          <div className="row">
            <div className="col-lg-6">
              <div className="apps">
                <h1>Download our app</h1>
                <p>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                  Quaerat excepturi provident minus labore quisquam accusantium,
                  dolorem similique tenetur fugiat repudiandae corrupti beatae
                  eos, dolorum explicabo quo, harum ab. Aliquam, fugit.
                </p>
                <div className="qr_image">
                  <img src={Image4} alt="qr code" />
                </div>
                <div className="downloads">
                  <div className="download_1">
                    <img src={Image5} alt="playstore" />
                  </div>
                  <div className="download_2">
                    <img src={Image6} alt="playstore" />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6">
              <div className="mobile">
                <img src={Image7} alt="mobile" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Home;
