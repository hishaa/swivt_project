import "./App.css";
import Topheader from "./Components/Topheader/Topheader";
import { Homerouting } from "./HomeRouting/Homerouting";
import Navbar from "./Components/Navbar/Navbar";
import Footer from "./Components/Footer/Footer";

function App() {
  return (
    <>
      <Topheader />
      <Navbar />

      <Homerouting />
      <Footer />
    </>
  );
}

export default App;
