import React from "react";

const Topheader = () => {
  return (
    <>
      <div className="topheader">
        <div className="container">
          <div className="row ">
            <div className="col-lg-6 col-md-6 t_front">
              <h6>24 Hr Service | 365 days</h6>
            </div>
            <div className="col-lg-6 col-md-6 t_back">
              <h6>Translate</h6>
              <h6>Support</h6>
              <h6>Help</h6>
              <h6>FQA</h6>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Topheader;
