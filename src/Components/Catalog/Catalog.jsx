import React from "react";
import "./catalog.css";

const Catalog = ({ image, text }) => {
  return (
    <>
      <div className="catalog_item">
        <div className="item_image">
          <img src={image} alt="items" />
        </div>
        <div className="title">
          <p>{text}</p>
        </div>
      </div>
    </>
  );
};

export default Catalog;
