import React from "react";
import { Link } from "react-router-dom";
import Logo from "../Assets/Images/logo.jpg";

const Navbar = () => {
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container">
          <div className="logo">
            <Link to="/">
              <img src={Logo} alt="logo" />
            </Link>
          </div>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mb-2 mb-lg-0">
              <li className="nav-item">
                <a className="nav-link " aria-current="page" href="#">
                  <i class="fa-solid fa-phone"></i>
                  <span>+977:98998989</span>
                </a>
                <a className="nav-link" aria-current="page" href="#">
                  <i class="fa-solid fa-location-dot"></i>
                  <span className="location">kathmandu,Nepal</span>
                </a>
              </li>
            </ul>
            <div className="input_form">
              <form className="d-flex">
                <input
                  className="form-control "
                  type="search"
                  placeholder="search by food"
                  aria-label="Search"
                />
                <span class="input-group-text">
                  <i class="fa-solid fa-magnifying-glass"></i>
                </span>
                <button className="btn" type="submit">
                  Login
                </button>
              </form>
            </div>
            <div className="add_to_cart">
              <i class="fa-solid fa-bag-shopping"></i>
              <sup className="cart-sup">1</sup>
            </div>
          </div>
        </div>
      </nav>
    </>
  );
};
export default Navbar;
