import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";

import "./carousel.css";
import Image from "../Assets/Images/food.png";

const Carousel = () => {
  var settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <Slider {...settings}>
      <div>
        <div className="contents">
          <div className="content_titles">
            <h6>all delicacies</h6>
            <h1>
              grilled <br />
              lamb
            </h1>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Repudiandae tempora non alias ipsa tenetur fuga deleniti officiis
              assumenda dolore natus cumque repellat perspiciatis labore rem
              ducimus possimus, molestiae culpa voluptas.
            </p>

            <button type="button" class="btn btn-sm">
              Add to Cart
            </button>
          </div>
          <div className="content_image">
            <img src={Image} alt="food" />
          </div>
        </div>
      </div>
      <div>
        <div className="contents">
          <div className="content_titles">
            <h6>all delicacies</h6>
            <h1>
              grilled <br />
              lamb
            </h1>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Repudiandae tempora non alias ipsa tenetur fuga deleniti officiis
              assumenda dolore natus cumque repellat perspiciatis labore rem
              ducimus possimus, molestiae culpa voluptas.
            </p>

            <button type="button" class="btn btn-sm">
              Add to Cart
            </button>
          </div>
          <div className="content_image">
            <img src={Image} alt="food" />
          </div>
        </div>
      </div>
    </Slider>
  );
};

export default Carousel;
