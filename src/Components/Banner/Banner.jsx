import React from "react";
import Image from "../Assets/Images/banner.png";
import "./banner.css";
export const Banner = () => {
  return (
    <>
      <div className="container banner">
        <img src={Image} alt="Banner Image" />
      </div>
    </>
  );
};
