import React from "react";
import "./coupen.css";

const Coupen = ({ backgroundcolor, offpercent, code }) => {
  return (
    <>
      <div className="coupons">
        <div
          className="coupon-card"
          style={{ backgroundColor: backgroundcolor }}
        >
          <h1>{offpercent}</h1>
          <div className="coupon-code clearfix">
            <h6>
              code: <br />
              {code}
            </h6>
            <div className="coupon-btn ">
              <button type="button" class="btn btn-sm">
                copy code
              </button>
            </div>
          </div>
          <div className="circle1"></div>
          <div className="circle2"></div>
        </div>
      </div>
    </>
  );
};
export default Coupen;
