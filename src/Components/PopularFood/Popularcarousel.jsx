import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import PopularFood from "./PopularFood";
import Popularlist from "./Popularlist";

const Popularcarousel = () => {
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          dots: true,
          infinite: true,
          speed: 500,
          slidesToShow: Popularlist.length > 4 ? 4 : Popularlist.length,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 767,
        settings: {
          dots: true,
          infinite: true,
          speed: 500,
          slidesToShow: Popularlist.length > 2 ? 2 : Popularlist.length,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          dots: true,
          infinite: true,
          speed: 500,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          dots: true,
          infinite: true,
          speed: 500,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <>
      {Popularlist?.length > 0 && (
        <Slider {...settings}>
          {Popularlist.map((item) => {
            return (
              <div>
                <PopularFood
                  image={item.image}
                  description={item.description}
                  pprice={item.pprice}
                  dprice={item.dprice}
                  discount={item.discount ? item.discount : null}
                />
              </div>
            );
          })}
        </Slider>
      )}
    </>
  );
};

export default Popularcarousel;
