import React from "react";
import "./popularfood.css";
const PopularFood = ({ image, dprice, pprice, description, discount }) => {
  return (
    <>
      <div className="popular-foods">
        <div className="card">
          <img src={image} className="card-img-top" alt="popular" />
          <div className="card-body">
            <p className="card-text">{description}</p>
          </div>
          <div className="card-footer">
            <div className="action-container d-flex justify-content-between align-items-center">
              <div className="price">
                <span>Rs. {dprice}</span> <br />
                <small>
                  <del>Rs.{pprice}</del>
                </small>
              </div>
              <div className="cart-button">
                <div className="d-grid gap-2">
                  <button
                    className="btn btn-outline-warning btn-sm"
                    type="button"
                  >
                    ADD
                  </button>
                </div>
              </div>
            </div>
          </div>
          {discount && (
            <p className="ribbon">
              <span className="text">
                <strong className="bold">{discount}</strong>
              </span>
            </p>
          )}
        </div>
      </div>
    </>
  );
};

export default PopularFood;
