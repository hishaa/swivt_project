import React from "react";
import { Link } from "react-router-dom";
import Partnerlist from "./Partnerlist";

const Footer = () => {
  return (
    <>
      <div className="footer ">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 col-md-6">
              <h3>Categories</h3>
              <div className="row">
                <div className="col-lg-6 col-md-6 col-6">
                  <ul className="menues">
                    <li>Breakfast Menu</li>
                    <li>soup</li>
                    <li>organic salad</li>
                    <li>signature pizza (12 inch)</li>
                    <li>half & half pizza</li>
                    <li>pizza</li>
                    <li>extra topping for pizza</li>
                    <li>antipasti</li>
                    <li>spaghetti</li>
                    <li>spaghetti or penne</li>
                    <li>Risotto</li>
                  </ul>
                </div>
                <div className="col-lg-6 col-md-6 col-6">
                  <ul className="menues">
                    <li>Desserts</li>
                    <li>oven baked dishes</li>
                    <li>bottle pickle chilly</li>
                    <li>extra</li>
                    <li>signature pizza (12 inch)</li>

                    <li>extra topping for pizza</li>
                    <li>antipasti</li>

                    <li>spaghetti or penne</li>
                    <li>soup</li>
                    <li>lunch menu</li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-md-6">
              <h3>Useful links</h3>
              <div className="row">
                <div className="col-lg-4 col-md-4 col-4">
                  <ul className="menues">
                    <li>
                      <Link to="/">about</Link>
                    </li>
                    <li>
                      {" "}
                      <Link to="/">careers</Link>
                    </li>
                    <li>
                      <Link to="/">blog</Link>
                    </li>
                    <li>
                      <Link to="/">press</Link>
                    </li>
                    <li>
                      <Link to="/">lead</Link>
                    </li>
                    <li>
                      <Link to="/">value</Link>
                    </li>
                  </ul>
                </div>
                <div className="col-lg-4 col-md-4 col-4">
                  <ul className="menues">
                    <li>
                      <Link to="/">privacy</Link>
                    </li>
                    <li>
                      <Link to="/">terms</Link>
                    </li>
                    <li>
                      <Link to="/">FAQS</Link>
                    </li>
                    <li>
                      <Link to="/">security</Link>
                    </li>
                    <li>
                      <Link to="/">mobile</Link>
                    </li>
                    <li>
                      <Link to="/">contact</Link>
                    </li>
                  </ul>
                </div>
                <div className="col-lg-4 col-md-4 col-4">
                  <ul className="menues">
                    <li>
                      <Link to="/">partner</Link>
                    </li>
                    <li>
                      <Link to="/">express</Link>
                    </li>
                    <li>
                      <Link to="/">local</Link>
                    </li>
                    <li>
                      <Link to="/">spotlight</Link>
                    </li>
                    <li>
                      <Link to="/">warehouse</Link>
                    </li>
                    <li>
                      <Link to="/">deliver</Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="p_partners ">
            <h3>payment partners</h3>
            <div className="row">
              {Partnerlist.map((items) => {
                return (
                  <div className="col-lg-3 col-md-3">
                    <div className="p_method">
                      <div className="P_image">
                        <img src={items.image} alt="payment method" />
                      </div>
                      <div className="p_text">
                        <h3>{items.title}</h3>
                        <p>{items.subtitle}</p>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="last_section">
            <div className="last_comp">
              <p>@ test private limited 2022</p>
            </div>

            <div className="terms">
              <p>terms and condition</p>
              <p>privay policy</p>
              <div className="social_icons">
                <i class="fa-brands fa-facebook-f"></i>
                <i class="fa-brands fa-instagram"></i>
                <i class="fa-brands fa-twitter"></i>
                <i class="fa-brands fa-linkedin-in"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
