import React from "react";

const partner = ({ image, title, subtitle }) => {
  return (
    <>
      <div className="p_method">
        <div className="P_image">
          <img src={image} alt="payment method" />
        </div>
        <div className="p_text">
          <h3>{title}</h3>
          <p>{subtitle}</p>
        </div>
      </div>
    </>
  );
};

export default partner;
