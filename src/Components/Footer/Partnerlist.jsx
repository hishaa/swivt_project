const Partnerlist = [
  {
    image: "./images/esewa.png",
    title: "esewa",
    subtitle: "pay using esewa digital wallet",
  },
  {
    image: "./images/khalti.png",
    title: "khalti",
    subtitle: "pay using khalti digital wallet",
  },
  {
    image: "./images/imepay.png",
    title: "imepay",
    subtitle: "pay using imepay digital wallet",
  },
];
export default Partnerlist;
