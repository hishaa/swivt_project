import React from "react";
import "./offer.css";
import Image from "../Assets/Images/delivery.jpg";
const Offers = ({ image, title, description }) => {
  return (
    <>
      <div className="offer mt-4">
        <div className="offer_image">
          <img src={image} alt="offer" />
        </div>
        <div className="offer_text">
          <h1>{title}</h1>
          <p>{description}</p>
        </div>
      </div>
    </>
  );
};

export default Offers;
