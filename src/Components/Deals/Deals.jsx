import React from "react";
import "./deal.css";

const Deals = ({ image }) => {
  return (
    <>
      <div className="best-deals">
        <div className="card">
          <img src={image} className="card-img-top" alt="Best Deal" />
        </div>
      </div>
    </>
  );
};

export default Deals;
