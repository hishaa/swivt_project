import React from "react";
import "./promotion.css";
import Image from "../Assets/Images/promo.png";

const Promotion = () => {
  return (
    <>
      <div
        className="promo_comp mt-4"
        // style={{ backgroundImage: "url(./images/2.png)" }}
      >
        <div className="promo_img">
          <img src={Image} alt="promotion" />
        </div>

        <div className="promo_text">
          {" "}
          <h4>promotion of the day!!!</h4>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Tenetur
            quis veniam quaerat magni animi reiciendis aliquid laboriosam
            similique beatae harum adipisci illum quam iure voluptatibus, quos
            voluptatum atque corrupti! Eius?
          </p>
        </div>
      </div>
    </>
  );
};

export default Promotion;
